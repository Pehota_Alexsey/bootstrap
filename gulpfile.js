const gulp = require('gulp');
// const autoprefixer = require('gulp-autoprefixer');
// const livereload = require('gulp-livereload');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const refresh = require('gulp-refresh')

gulp.task('hello', function() {
    console.log('Hello Zell');
});

// gulp.task('sass', function() {
//   return gulp.src('src/blocks/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
//     .pipe(sass())
//     .pipe(gulp.dest('build/css'))
// })

gulp.task('all-sass', function() {
 return gulp.src('./src/blocks/**/*.scss')
    .pipe(concat('main.scss'))
    .pipe(gulp.dest('./src'));
    
 });


 gulp.task('scss', function() {
  return gulp.src('src/main.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('build/css'))
    .pipe(refresh());
});

gulp.task('watch', function () {
    gulp.watch('src/blocks/**/*.scss', ['scss']);
});

// gulp.task('css', function() {
//     return gulp.src('./src/blocks/**/*.scss')
//     .pipe(concat('main.scss'))
//     .pipe(gulp.dest('./src'))
//     .pipe(gulp.src('src/main.scss')) // Gets all files ending with .scss in app/scss and children dirs
//     .pipe(sass())
//     .pipe(gulp.dest('build/css'))
//     .pipe(refresh());
//   });

// gulp.task('watch', () => {
//     refresh.listen();
//     gulp.watch('src/blocks/**/*.scss', ['scss']); //['all-sass']);
//   });
  

// gulp.task('watch', () => {
//   refresh.listen();
//   gulp.watch('src/blocks/**/*.scss', ['all-sass', 'scss']);
// });


/*
gulp.task('sass', function(){
    //console.log(10);
    gulp.src('./src/** /*.scss')
        .pipe(sass().on('error', sass.logError))
           .pipe(gulp.dest('./src'))
});

gulp.task('sass:watch', function(){
    gulp.watch('./src/** /*.scss',['sass']);
});

/*
gulp.task('watch', function () {
    // Watch .css files
    gulp.watch('temp/foxy/css/** /*.css', ['minify-concat-styles']);
    // Watch .js files
    gulp.watch('temp/foxy/js/** /*.js', ['uglify-concat-scripts']);
});
*/